/**
 * Created by adhodi on 09/07/15.
 */
$("#searchBox").on("keypress",function(e){
    if(e.which==13){
        e.preventDefault();
        var _searchKey = (this.value).toLowerCase();
        console.log("search for "+_searchKey );
        var _found=false;
        $(".chair.selected").removeClass("selected");
        getByFirstName(_searchKey,function(_result){
            if(!!_result) {
                var _e = $("#" + _result.eid);
                if(_e.length>0) {
                    _found = true;
                    $(_e).addClass("selected");
                    var _left = $(_e).position().left + 93;
                    var _top = $(_e).position().top;
                    var _parent = $(_e).parent();
                    var _emp = new Employe(_result);
                    $("#eDetails").html(_emp.getHtml());
                    $("#eDetails").css({left: _left, top: _top}).show();
                    $(_parent).append($("#eDetails"));
                }
            }else{
                getByLastName(_searchKey,function(_result){
                    if(!!_result && _result.length>0) {
                        console.log("total results "+_result.length);
                        for(var i =0;i<_result.length;i++) {
                            var _e = $("#" + _result[i].eid);
                            if (_e.length > 0) {
                                _found = true;
                                $(_e).addClass("selected");
                                if(_result.length==1) {
                                    var _left = $(_e).position().left + 93;
                                    var _top = $(_e).position().top;
                                    var _parent = $(_e).parent();
                                    var _emp = new Employe(_result);
                                    $("#eDetails").html(_emp.getHtml()).show();
                                    $("#eDetails").css({left: _left, top: _top});
                                    $(_parent).append($("#eDetails"));
                                }
                            }
                        }
                    }else{
                        if(!_found)
                            $("#error").html("No results found");
                    }
                });
            }
        });
    }
});
$(document.body).on("mouseenter",".chairOccupied",function(){
    clearTimeout(_timeRef);
    var _id = $(this).attr("id");
    var _left = $(this).position().left + 93;
    var _top = $(this).position().top + 5;
    var _parent = $(this).parent();
    $("#eDetails").hide();
    findByKey(_id,function(_result){
        var _emp = new Employe(_result);
        $("#eDetails").css({left:_left,top:_top}).show();
        $("#eDetails").html(_emp.getHtml());
        $(_parent).append($("#eDetails"));
    });
});

function createFilters(){
    var _html="<div class='filterWrap'>";
    var _teams = Teams;
    var _desg  = Designations;
    _html +="<div class='filterOptions'>" +
        "<div id='byTeams' class='selected'><span id='byteamCheck' ></span><span id='byTeamTitle'>Team</span></div>" +
        "<div id='byJobTitle'><span id='byjobtitlechk'></span><span id='byJobTitleTitle'>Job Title</span></div>" +
        "</div>";
    _html +="<div id='teamCol'>";
    for(var i = 0;i<_teams.length;i++){
        _html+="<div class='teamName'>"+_teams[i].team+"</div>";
    }
    _html +="</div>";
    _html +="<div id='desigCol'>";
    for(var i = 0;i<_desg.length;i++){
        _html+="<div class='desigName'>"+_desg[i].designation+"</div>";
    }
    _html +="</div>";
    _html +="<div id='doFilter'> do Filter </div>";
    _html +="<div id='clearFilter'> clear Filter </div></div>";
    return _html;
}
$("#filterCont").html(createFilters());
(function(){
    $(document.body).on("click",".teamName,.desigName",function(e){
        e.stopPropagation();
        $(this).toggleClass("selected");
    }).on("click","#doFilter",function(e){
        e.stopPropagation();
        var _desigs=$(".desigName.selected");
        var _teams=$(".teamName.selected");
        var _desigParam=[];
        var _teamParam=[];
        for(var i = 0;i<_desigs.length;i++){
            _desigParam.push($(_desigs[i]).html());
        }
        for(var j = 0;j<_teams.length;j++){
            _teamParam.push($(_teams[j]).html());
        }
        getTeamWize(_teamParam,function(_result){
            var _e = $("#"+_result.eid);
            if(_e.length>0) {
                $(_e).addClass("selected");
            }
        });
        getDesigWize(_desigParam,function(_result){
            var _e = $("#"+_result.eid);
            if(_e.length>0) {
                $(_e).addClass("selected");
            }
        });
    }).on("click","#byTeams",function(e){
        e.stopPropagation();
        $(this).toggleClass("selected");
        if($(this).hasClass("selected")){
            $("#teamCol").show();
            $("#desigCol").hide();
            $("#byjobtitlechk").removeClass("selected");
        }else
            $("#teamCol").hide();
    }).on("click","#byjobtitlechk",function(e){
        e.stopPropagation();
        $(this).toggleClass("selected");
        if($(this).hasClass("selected")){
            $("#desigCol").show();
            $("#teamCol").hide();
            $("#byTeams").removeClass("selected");
        }else
            $("#desigCol").hide();
    }).on("click","#filter",function(e){
        e.stopPropagation();
        $(".filterWrap").show();
    }).on("click","#clearFilter",function(e){
        e.stopPropagation();
        $(".chair.selected").removeClass("selected");
        $(".teamName.selected").removeClass("selected");
        $(".desigName.selected").removeClass("selected");
    }).on("mouseleave",".filterWrap",function(){
        $(this).hide();
    }).on("click",function(){
        $(".filterWrap").hide();
    }).on("mouseleave",".chairOccupied,#eDetails",function(e){
        _timeRef = setTimeout(function(){$("#eDetails").hide();},100);
    }).on("mouseenter","#eDetails",function(e){
        clearTimeout(_timeRef);
        $(this).show();
    });
})();
var _timeRef;
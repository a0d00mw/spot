/**
 * Created by adhodi on 05/07/15.
 */


var TOTALCHAIRS=4;
var emps = [];
var tables=[];
var empOObjs=[];
var tablesObj=[];

function buildUI(){
    getAllData("spot",function(_result){
        emps=_result;
        getAllData("tables",function(_result) {
            tables = _result;


            for (var i = 0; i < emps.length; i++) {
                empOObjs[emps[i].eid] = new Employe(emps[i]);
            }

            for (var t = 0; t < tables.length; t++) {
                tablesObj.push(new Table(tables[t]));
            }

            var _floor = new Floor();
            _floor.setTables(tablesObj);
            _floor.setTotalEmp(empOObjs.length);
            _floor.setVaccantSeats(0);

            $("#seatingArrangement").html(_floor.getHtml());
            bindDrag();

            console.log(empOObjs);
            console.log(tablesObj);
            empOObjs = null;
            tablesObj = null;
        });
    });
}

function Floor(obj){
    var tables;
    var totalEmp;
    var vaccantSeats;

    if(!!obj){
        tables=obj.tables;
        totalEmp=obj.totalEmp;
        vaccantSeats=obj.vaccantSeats;
    }

    this.getTables=function(){return tables;};
    this.setTables=function(_tables){tables=_tables};
    this.getTotalEmp=function(){return totalEmp;};
    this.setTotalEmp=function(_totalEmp){totalEmp=_totalEmp};
    this.getVaccantSeats=function(){return vaccantSeats;};
    this.setVaccantSeats=function(_vaccantSeats){vaccantSeats=_vaccantSeats};

}

Floor.prototype.getHtml=function(){
    var tableErr= this.getTables();
    var _html="";
    for(var i = 0;i<tableErr.length;i++){
        _html +="<div id='table_"+tableErr[i].getTableId()+"' class='tablesWrap'>";
        _html += tableErr[i].getHtml();
        _html +="</div>";
    }
    return _html;
};

function Table(obj){
    var tableId;
    var totalChairs;
    var seq;

    if(!!obj){
        tableId = obj.tableId;
        totalChairs = obj.totalChairs;
        seq = obj.seq;
        console.log(seq);
    }


    this.getTotalChairs = function(){return totalChairs};
    this.getSeq = function(){return seq};
    this.setTotalChairs = function(_totalChairs){totalChairs = _totalChairs};
    this.setSeq = function(_seq){seq = _seq};
    this.setTableId = function(_tableId){tableId = _tableId};
    this.getTableId = function(){return tableId};
}

Table.prototype.getHtml=function(){
    var _html="";
    var emparr = this.getSeq().split(",");
    var currEmp;
    for(var i = 0;i<TOTALCHAIRS;i++){
        currEmp = empOObjs[emparr[i]];
        if(!!currEmp) {
            _html += "<div draggable='true' id='" + currEmp.getEid() + "' class='chair chairOccupied chair_"+i+"'>" +
                     "</div>";
        }else{
            _html += "<div draggable='true' class='chair chairEmpty chair_"+i+"'>" +
                     "</div>";
        }
    }
    _html +="<div class='tableSpread'>Table "+this.getTableId()+"</div>";
    return _html;
};

function Employe(obj){
    var eid;
    var fname;
    var lname;
    var designation;
    var email;
    var contactno;
    var table;
    var project;
    var team;
    var imgSrc;

    if(!!obj){
        eid             = obj.eid;
        fname           = obj.fname;
        lname           = obj.lname;
        designation     = obj.designation;
        email           = obj.email;
        contactno       = obj.contact;
        table           = obj.Table;
        project         = obj.CurrentProject;
        team            = obj.Team;
        imgSrc          = obj.imgSrc;
    }

    this.setEid =function(_eid){eid=_eid;};
    this.getEid =function(){return eid;};
    this.setFName =function(_fname){fname=_fname;};
    this.setLName =function(_lname){lname=_lname;};
    this.getFName =function(){return fname};
    this.getLName =function(){return lname};
    this.setDesignation =function(_designation){designation=_designation};
    this.getDesignation =function(){return designation};
    this.setEmail =function(_email){email=_email};
    this.getEmail =function(){return email};
    this.setContactno =function(_contactno){contactno=_contactno};
    this.getContactno =function(){return contactno};
    this.setTable =function(_table){table=_table};
    this.getTable =function(){return table};
    this.setProject =function(_project){project=_project};
    this.getProject =function(){return project};
    this.getImgSrc =function(){return imgSrc};
    this.getTeam =function(){return team};
    this.setTeam =function(_team){team=_team};
    this.setImgSrc =function(_imgsrc){imgSrc=_imgsrc};
}

Employe.prototype.getHtml=function(){
    var _html = "<div class='empWrap'>";
    _html += "<div class='rightArrow'>" +
        "</div><div class='rightArrowBack'></div><div class='empDetails'>" +
        "<div class='imgWrap'></div>" +
        "<div class='nameDesig'><span class='ename'>"+this.getFName()+"&nbsp;"+this.getLName()+"</span><span class='desig'>" +
        ""+this.getDesignation()+"</span> </div>" +
        "<div class='team'><div class='teamTitle'>Team</div><div class='teamName'>"+this.getTeam()+"</div> </div>" +
        "<div class='project'><div class='ptitle'>Project</div><div class='pname'>"+this.getProject()+"</div></div>" +
        " </div>";
    _html +="</div>";
    return _html;
};


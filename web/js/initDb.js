/**
 * Created by adhodi on 05/07/15.
 */
window.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
window.IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction || window.msIDBTransaction;
window.IDBKeyRange = window.IDBKeyRange || window.webkitIDBKeyRange || window.msIDBKeyRange;

    var db;


    var DBOpenRequest = window.indexedDB.open("spot",10);

    DBOpenRequest.onerror = function (event) {
        console.log("dbopenrequest error");
    };

    DBOpenRequest.onsuccess = function (event) {
        console.log("dbopenrequest success");
        db = DBOpenRequest.result;
        addData(event);
        addTables(event);
        buildUI();
    };

    DBOpenRequest.onupgradeneeded = function (event) {
        var db = event.target.result;

        db.onerror = function (event) {
            console.log('object store error');
        };


        var tableStore = db.createObjectStore("tables", { keyPath: "tableId" });
        tableStore.createIndex("totalChairs", "totalChairs", { unique: false});
        tableStore.createIndex("seq", "seq", { unique: false});

        var objectStore = db.createObjectStore("spot", { keyPath: "eid" });
        objectStore.createIndex("eid", "eid", { unique: true});
        objectStore.createIndex("Team", "Team", { unique: false });
        objectStore.createIndex("fname", "fname", { unique: false });
        objectStore.createIndex("lname", "lname", { unique: false });
        objectStore.createIndex("designation", "designation", { unique: false });
        objectStore.createIndex("CurrentProject", "CurrentProject", { unique: false });
        objectStore.createIndex("Table", "Table", { unique: false });
        objectStore.createIndex("email", "email", { unique: true });
        objectStore.createIndex("contact", "email", { unique: true });
        objectStore.createIndex("imgSrc", "imgSrc", { unique: false });


        console.log('--------------Object store created');
    };

    function getAllData(storeName,callback) {
        var objectStore = db.transaction(storeName).objectStore(storeName);
        var _result = [];
        objectStore.openCursor().onsuccess = function (event) {
            var cursor = event.target.result;
            if (cursor) {
                console.log(cursor.value.eid);
                _result.push(cursor.value);
                cursor.continue();
            } else {
                console.log("cursor null");
                callback(_result);
            }
        }
    }

    function addData(e) {
        e.preventDefault();

        var transaction = db.transaction(["spot"], "readwrite");

        transaction.oncomplete = function () {
            console.log("xsantion complete");
        };

        transaction.onerror = function () {
            console.log("xsantion error");
        };

        var objectStore = transaction.objectStore("spot");
        console.log(objectStore.keyPath + " keypath");

        var objectStoreRequest;
        try {
            for (var i = 0; i < newItem.length; i++) {
                objectStoreRequest = objectStore.add(newItem[i]);
                objectStoreRequest.onsuccess = function (event) {
                    console.log("added " + i);
                };
            }
        }catch (e){}
    };
    function clear(){

        var transaction = db.transaction(["spot"], "readwrite");
        var objectStore = transaction.objectStore("spot");
        objectStore.clear();

        transaction = db.transaction(["tables"], "readwrite");
        objectStore = transaction.objectStore("tables");
        objectStore.clear();
    }
    function getTeamWize(teams,callback){
        var objectStore = db.transaction('spot').objectStore('spot');
        var index = objectStore.index("Team");
        equalsAnyOf(teams,callback,function(){console.log("done")},index);
    }
    function getDesigWize(desigs,callback){
        var objectStore = db.transaction('spot').objectStore('spot');
        var index = objectStore.index("designation");
        equalsAnyOf(desigs,callback,function(){console.log("done")},index);
    }
    function findByKey(key,callback) {
        var tx = db.transaction(["spot"], "readonly");
        var store = tx.objectStore("spot");
        var request = store.get(key);

        request.onsuccess = function (e) {
            callback(e.target.result);
        };
    }
    function getByFirstName(firstName,callback){
        var objectStore = db.transaction('spot').objectStore('spot');
        var index = objectStore.index("fname");
        var keyRangeValue = IDBKeyRange.only(firstName);

        index.openCursor(keyRangeValue).onsuccess = function(event) {
            var cursor = event.target.result;
            if(cursor) {
                console.log(cursor.value.eid);
                callback(cursor.value);
                cursor.continue();
            } else {
                console.log('Entries all displayed.');
                callback(null);
            }
        };
    }
    function getByLastName(lastname,callback){
        var objectStore = db.transaction('spot').objectStore('spot');
        var index = objectStore.index("lname");
        var keyRangeValue = IDBKeyRange.only(lastname);
        var _result=[];
        index.openCursor(keyRangeValue).onsuccess = function(event) {
            var cursor = event.target.result;
            if(cursor) {
                console.log(cursor.value.eid);
                _result.push(cursor.value);
//                callback(cursor.value);
                cursor.continue();
            } else {
                console.log('Entries all displayed.bylastname');
                callback(_result);
            }
        };
    }
    function equalsAnyOf(keysToFind, onfound, onfinish,index) {
        var set = keysToFind.sort();
        var i = 0;
        var cursorReq = index.openCursor(); // Assume 'index' exists in a parent closure scope
        cursorReq.onsuccess = function (event) {
            var cursor = event.target.result;
            if (!cursor) { onfinish(); return; }
            var key = cursor.key;
            while (key > set[i]) {
                // The cursor has passed beyond this key. Check next.
                ++i;
                if (i === set.length) {
                    // There is no next. Stop searching.
                    onfinish();
                    return;
                }
            }
            if (key === set[i]) {
                // The current cursor value should be included and we should continue
                // a single step in case next item has the same key or possibly our
                // next key in set.
                onfound(cursor.value);
                cursor.continue();
            } else {
                // cursor.key not yet at set[i]. Forward cursor to the next key to hunt for.
                cursor.continue(set[i]);
            }
        };
    }

    function addTables(event) {
        event.preventDefault();

        var transaction = db.transaction(["tables"], "readwrite");

        transaction.oncomplete = function () {
            console.log("table xsantion complete");
        };

        transaction.onerror = function () {
            console.log("table xsantion error");
        };

        var objectStore = transaction.objectStore("tables");
        console.log(objectStore.keyPath + " keypath table");

        var objectStoreRequest;
        console.log(_tables);
        try {
            for (var i = 0; i < _tables.length; i++) {
                objectStoreRequest = objectStore.add(_tables[i]);
                objectStoreRequest.onsuccess = function (event) {
                    console.log("added table" + i);
                };
            }
        }catch (e){}
    };


    function updateTable(_tableObj) {

        var transaction = db.transaction(["tables"], "readwrite");

        transaction.oncomplete = function () {
            console.log("table xsantion complete");
        };

        transaction.onerror = function () {
            console.log("table xsantion error");
        };

        var objectStore = transaction.objectStore("tables");
        var _req = objectStore.put(_tableObj);
        _req.onsuccess = function(e){
            console.log("db updated");
        };
        _req.onerror = function(e){
            console.log('Error adding: '+e);
        };
    };

    function updateEmp(_emp) {

        var transaction = db.transaction(["spot"], "readwrite");

        transaction.oncomplete = function () {
            console.log(" xsantion complete");
        };

        transaction.onerror = function () {
            console.log(" xsantion error");
        };

        var objectStore = transaction.objectStore("spot");
        var _req = objectStore.put(_emp);
        _req.onsuccess = function(e){
            console.log("db updated");
        };
        _req.onerror = function(e){
            console.log('Error adding: '+e);
        };
    };
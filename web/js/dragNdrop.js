/**
 * Created by adhodi on 08/07/15.
 */

function bindDrag() {
    $(".chair").on("dragstart", function (e) {
        handleStart(e.originalEvent, this);
    })
        .on("dragenter", function (e) {
            handledragenter(e.originalEvent, this);
        })
        .on("drop", function (e) {
            handledrop(e.originalEvent, this);
        })
        .on("dragover", function (e) {
            handledragover(e.originalEvent, this);
        })
        .on("dragleave", function (e) {
            handledragleave(e.originalEvent, this);
        })
        .on("dragend", function (e) {
            handledragend(e.originalEvent, this);
        }).on("mousedown",function(){
            $("#eDetails").hide();
        }).on("mouseenter",function(){
            if($(this).hasClass("chairOccupied"))
                $("#eDetails").show();
        }).on("mouseup",function() {
            $("#eDetails").show();
        });
}

var draggedEle;
var origDragEle;
var _eleDropped=false;
//DRAG_START
function handleStart(e, iEle) {
    console.log("start");
    if($(iEle).hasClass("chairEmpty"))
        return;
    $(iEle).addClass("dragging");
    $("#eDetails").hide();
    var dt = e.dataTransfer;
    dt.effectAllowed = 'move';
    dt.setData('text/html', $(iEle).html());
    draggedEle = iEle;
    origDragEle = iEle.cloneNode(true);
}

//DROP
function handledrop(e, iEle) {
    console.log("drop");
    $(iEle).removeClass("denter");
    var draggedId = draggedEle.id;

    draggedEle.id = iEle.id;
    if(!!draggedId)
        iEle.id=draggedId;

    if($(iEle).hasClass("chairEmpty")){
        $(draggedEle).removeClass("chairOccupied").addClass("chairEmpty");
        $(iEle).removeClass("chairEmpty").addClass("chairOccupied");
    }

    e.preventDefault();
    _eleDropped = true;
    updateThisTable(iEle);
    updateThisTable(draggedEle);
    updateThisEmp(iEle);
    updateThisEmp(draggedEle);

    var origTable = $(draggedEle).closest(".tablesWrap").find(".tableSpread").html();
    var newTable = $(iEle).closest(".tablesWrap").find(".tableSpread").html();
    if(origTable!=newTable) {
        var eid = draggedEle.id;
        findByKey(eid,function(_result){
            $("#notification").html(_result.fname + " has been moved from "+origTable+" to "+newTable+"" +
                ". A mail regarding this has been sent to "+_result.fname+ "!");
        });
    }

}

//DRAG_ENTER
function handledragenter(evt, iEle) {
    console.log("enter");
    if (draggedEle !== iEle) {
        $(iEle).addClass("denter");
    }
    evt.preventDefault();
    return true;
}

//DRAG_OVER
function handledragover(e, iEle) {
    console.log("over");
    clearTimeout(iEle.leaveref);
    $(iEle).addClass("denter");

    if (e.preventDefault) {
        e.preventDefault();
    }
    e.dataTransfer.dropEffect = 'move';
    return false;
}

//DRAG_LEAVE
function handledragleave(evt, iEle) {
    console.log("leave");
    iEle.leaveref = setTimeout(function(){$(iEle).removeClass("denter");},5)
}

//DRAG_END
function handledragend(evt, iEle) {
    console.log("end");
    _eleDropped = false;
    $(".dragging").removeClass("dragging");
}


function updateThisTable(ele){
    var _chairs  = $(ele).closest(".tablesWrap").find(".chair");
    var _seq = [];
    for(var i = 0;i<_chairs.length;i++){
        if(_chairs[i].id=="")
            _seq.push(null);
        else
            _seq.push(_chairs[i].id);
    }
    var _tableObj={tableId:$(ele).closest(".tablesWrap").attr("id").replace("table_",""),
                    seq:_seq.toString(),
            totalChairs:4};
    updateTable(_tableObj);
}

function updateThisEmp(ele){
    var _eid = ele.id;
    var _tableId = $(ele).closest(".tablesWrap").attr("id").replace("table_","");
    findByKey(_eid,function(_result){
        if(!!_result) {
            _result.tableId = _tableId;
            updateEmp(_result);
        }
    });
}